const fs = require('fs');
const axios = require('axios')

class Search {

	history = ['']
	dbPath = './db/database.json';


	get paramsMapbox() {
		return {
			'access_token': process.env.MAPBOX_KEY,
			'limit': 5,
			'language': 'es'
		}
	}

	get paramsOpenWeather() {
		return {
			appid: process.env.OPENWEATHER_KEY,
			lang: 'es',
			units: 'metric'

		}
	}

	get historyCapitalized() {
		return this.history.map(place => {
			let words = place.split(' ');
			words = words.map(p => p[0].toUpperCase() + p.substring(1))
			return words.join(' ');
		})
	}

	constructor() {
		//TODO: leer base de db si existe
		this.readDB()

	}


	async city(place = '') {

		try {
			// peticion http
			const instance = axios.create({
				baseURL: `https://api.mapbox.com/geocoding/v5/mapbox.places/${place}.json`,
				params: this.paramsMapbox
			});

			const resp = await instance.get();
			return resp.data.features.map(places => ({
				id: places.id,
				name: places.place_name,
				lng: places.center[0],
				lat: places.center[1]
			}));
		} catch (err) {
			console.log(err);
		}

		return []; // retornar los lugares
	}

	async weatherPlace(lat, lon) {
		try {
			const instance = axios.create({
				baseURL: `https://api.openweathermap.org/data/2.5/weather`,
				params: {...this.paramsOpenWeather, lat, lon}
			});
			const resp = await instance.get();
			const {main, weather} = resp.data;

			return {
				desc: weather[0].description,
				min: main.temp_min,
				max: main.temp_max,
				temp: main.temp,
			}
		} catch (err) {
			console.log(err);
		}
	}


	addToHistory(place = '') {

		if (this.history.includes(place.toLocaleLowerCase())) {
			return;
		}

		this.history.splice(0, 5);
		this.history.unshift(place.toLocaleLowerCase());
		this.saveDB();
	}


	saveDB() {

		const payload = {
			history: this.history
		}

		fs.writeFileSync(this.dbPath, JSON.stringify(payload))

	}

	readDB() {
		if (!fs.existsSync(this.dbPath)) return;

		const info = fs.readFileSync(this.dbPath, {encoding: 'utf-8'});
		const data = JSON.parse(info);
		this.history = data.history;

	}

}


module.exports = Search;
