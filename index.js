require('dotenv');

const {readInput, inquirerMenu, pause, listPlaces} = require('./helpers/inquirer');
const Search = require('./models/searchs');

const main = async () => {

	const searchs = new Search();
	let opt;


	do {

		opt = await inquirerMenu();

		switch (opt) {
			case 1:
				// Mostrar mensaje
				const searchTerm = await readInput('Ciudad: ');

				// Buscar los lugares
				const places = await searchs.city(searchTerm);

				//Seleccionar el lugar
				const id = await listPlaces(places);

				if (id === '0') {
					continue;
				}

				// Guardar en db
				searchs.addToHistory(searchTerm);


				const placeSelected = places.find(p => p.id === id)

				// Clima
				const weather = await searchs.weatherPlace(placeSelected.lat, placeSelected.lng);

				// Mostrar resultados

				console.clear();
				console.log('\nInformación de la ciudad\n'.green);
				console.log('Ciudad: ', placeSelected.name.green);
				console.log('Lat: ', placeSelected.lat);
				console.log('Lng: ', placeSelected.lng);
				console.log('Temperatura: ', weather.temp);
				console.log('Mínima: ', weather.min);
				console.log('Maxima: ', weather.max);
				console.log('Como está el clima: ', weather.desc.green);
				break;
			case 2:

				searchs.historyCapitalized.forEach((place, i) => {
					const idx = `${i + 1}.`.green;
					console.log(`${idx} ${place}`);
				})
				break;
		}

		if (opt !== 0) await pause();


	} while (opt !== 0)

}

main();
